package cmd

import (
	"fmt"
	"github.com/skip2/go-qrcode"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

// [pr,<format_version>,<mqtt_address>,<mqtt_user>,<mqtt_password>,<mqtt_topic>,<client_priv_key>,<server_pub_key>,<profile_name>,<profile_icon_url>]
const pairingStringFormat = "[pr,1,%s,%s,%s,%s,%s,%s,%s,%s]"

// pairCmd represents the pair command
var pairCmd = &cobra.Command{
	Use:   "pair",
	Short: "Show pairing information for the selected profile",
	Long:  `Show pairing information needed for clients to subscribe to notification specific to the selected profile.`,
	Run: func(cmd *cobra.Command, args []string) {
		pairingString := fmt.Sprintf(pairingStringFormat,
			viper.GetString("address"),
			viper.GetString("client_user"),
			viper.GetString("client_password"),
			viper.GetString("topic"),
			viper.GetString("client_priv_key"),
			viper.GetString("server_pub_key"),
			strings.Split(filepath.Base(viper.ConfigFileUsed()), ".")[0], // Extract the profile name
			viper.GetString("profile_icon_url"),
		)
		switch cmd.Flag("format").Value.String() {
		case "text":
			fmt.Println(pairingString)
		case "qrcode":
			var png []byte
			qrSize, err := strconv.Atoi(cmd.Flag("qrcode-size").Value.String())
			if err != nil {
				fmt.Println("Can not parse qrcode-size: ", err.Error())
			}
			png, err = qrcode.Encode(pairingString, qrcode.Medium, qrSize)
			if err != nil {
				println("Error encoding qrcode. Exiting")
				os.Exit(1)
			}

			os.Stdout.Write(png)
		}

	},
}

func init() {
	rootCmd.AddCommand(pairCmd)
	pairCmd.PersistentFlags().StringP("format", "f", "text", `Pairing format to output. Can be 'text' or 'qrcode'.
Note that when using qrcode format, png formated qrcode will be sent to stdout. 
You can pipe that to a file or to 'display' command from imagemagick to show it in screen
by running: pushrocket pair -f qrcode | display`)
	pairCmd.PersistentFlags().Uint("qrcode-size", 512, "Set qrcode image size. Only used when format is qrcode.")
}
