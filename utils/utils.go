package utils

import (
	"encoding/json"
	"fmt"
	"github.com/eclipse/paho.mqtt.golang"
	"gitlab.com/ahmida/pushrocket/crypto"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

type Profile struct {
	Address        string `yaml:"address"`
	ClientUser     string `yaml:"client_user"`
	ClientPassword string `yaml:"client_password"`
	Topic          string `yaml:"topic"`
	ClientPrivKey  string `yaml:"client_priv_key"`
	ServerPubKey   string `yaml:"server_pub_key"`
	ProfileName    string `yaml:"profile_name"`
	ProfileIconURL string `yaml:"profile_icon_url"`
	Command        string `yaml:"command"`
	MqttListener   mqtt.Client `yaml:"-"`
}

type Notification struct {
	Id            string `json:"id,omitempty"`
	SchemaVersion int    `json:"schema_version,omitempty"`
	DateTime      string `json:"date_time,omitempty"`
	Title         string `json:"title,omitempty"`
	Icon          string `json:"icon,omitempty"` //This is the icon url
	Message       string `json:"message"`
}

func (p *Profile) Listen(ClientID string) error {
	opts := mqtt.NewClientOptions().AddBroker(p.Address).SetClientID(ClientID).SetCleanSession(false)
	opts.SetKeepAlive(2 * time.Second)
	opts.SetPingTimeout(1 * time.Second)
	opts.SetDefaultPublishHandler(func(client mqtt.Client, msg mqtt.Message) {
		if msg.Topic() == p.Topic {
			priv, err := crypto.StringToKey(p.ClientPrivKey)
			if err != nil {
				fmt.Println("Can not decrypt message. Error parsing client private key: ", err.Error())
			}
			key := crypto.Keypair{Private: *priv}
			pub, err := crypto.StringToKey(p.ServerPubKey)
			if err != nil {
				fmt.Println("Can not decrypt message. Error parsing server public key: ", err.Error())
			}
			notifJSON, ok := key.Decrypt(pub, msg.Payload())
			if !ok {
				fmt.Println("Can not decrypt notification payload. Check public and private key.")
				return
			}
			// fmt.Println(string(notifJSON))
			var notif Notification
			if err := json.Unmarshal(notifJSON, &notif); err != nil {
				fmt.Println("Can not unmarshal notification json data: ", err.Error())
				return
			}
			output, err := ExecTemplate(p.Command, notif, p.ProfileName, p.ProfileIconURL)
			if err != nil {
				fmt.Println("Error executing command template: ", err.Error())
				return
			}
			fmt.Print(string(output))
		}
	})

	p.MqttListener = mqtt.NewClient(opts)
	if token := p.MqttListener.Connect(); token.Wait() && token.Error() != nil {
		return token.Error()
	}

	if token := p.MqttListener.Subscribe(p.Topic, 2, nil); token.Wait() && token.Error() != nil {
		return token.Error()
	}
	return nil
}

func ExecTemplate(cmd string, notif Notification, profileName string, profileIconUrl string) ([]byte, error) {
	formattedCmd := strings.Replace(cmd, "{{id}}", notif.Id, -1)
	formattedCmd = strings.Replace(formattedCmd, "{{version}}", strconv.Itoa(notif.SchemaVersion), -1)
	formattedCmd = strings.Replace(formattedCmd, "{{date_time}}", notif.DateTime, -1)
	formattedCmd = strings.Replace(formattedCmd, "{{title}}", notif.Title, -1)
	formattedCmd = strings.Replace(formattedCmd, "{{notif_icon_url}}", notif.Icon, -1)
	formattedCmd = strings.Replace(formattedCmd, "{{message}}", notif.Message, -1)
	formattedCmd = strings.Replace(formattedCmd, "{{profile_name}}", profileName, -1)
	formattedCmd = strings.Replace(formattedCmd, "{{profile_icon_url}}", profileIconUrl, -1)
	return exec.Command("sh", "-c", formattedCmd).Output()
}
