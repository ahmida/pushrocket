# PushRocket

Self hosted and fully encrypted notification sending and receiving tool alternative to pushbullet that uses MQTT under the hood.

**WARNING : This software is an alpha proof-of-concept version. I do not recommend usage for production yet**

## How to install

Make sure that you have [Go](https://golang.org/doc/install) installed in your system then run the following:

```
$ go get gitlab.com/ahmida/pushrocket
$ go build gitlab.com/ahmida/pushrocket
$ go install gitlab.com/ahmida/pushrocket
```

`pushrocket` and `pullrocket` should be added to `$GOPATH/bin`

## How to use this
### 1. Simple usage

Make sure you have a MQTT server that you can use to test this. If you don't have one you can use [eclipse-mosquitto docker container](https://hub.docker.com/_/eclipse-mosquitto/) by running the following commands:

```
$ docker pull eclipse-mosquitto
$ docker run -it -p 1883:1883 -p 9001:9001 eclipse-mosquitto
```

Now, let's play with pushrocket:

```
$ # Generate a new default profile with pushrocket:
$ pushrocket config --address "tcp://localhost:1883"

$ # Generate a pairing string that we need to send to pullrocket to be able to receive notifications fromA this profile:
$ pushrocket pair
<pairing_string_printed>

$ # Now let's add this profile to pullrocket. Make sure you correctly escape characters with `''`:
$ pullrocket pair '<pairing_string_printed>'

$ # Start listening for incoming notifications with pullrocket:
$ pullrocket pull

$ # Send a notification form pushrocket:
$ pushrocket push '{"title": "Test notification", "message": "This is a test notification"}'
```

### 2. Execute a specific command when new notification received

In this example we will see how to run a specific linux command when a new notification is received in pullrocket:

```
$ # Let's clean the old profile from pushrocket and pullrocket first:
$ # Please be very careful when running this command to avoid deleting your home directory or burning your house.
$ # Do this at your own risk. I am not responsible for any data loss.

$ rm -r ~/.config/pushrocket

$ pushrocket config --address "tcp://localhost:1883"
$ pushrocket pair
<pairing_string_printed>

$ # This will show incoming notifications in your linux desktop:
$ pullrocket config '<pairing_string_printed>' --cmd 'notify-send "{{title}}" "{{message}}"'

$ # Let send a notification:
$ pushrocket push '{"title": "Hello world", "message": "Hello world notification"}'
```

## Understanding profiles:

Before we keep diving on pushrocket usage examples, we need to understand profiles and why they are power full.

Let's imagine those use cases: 

- You want to receive some notifications on your laptop and some others (or all other) in your phone.
- You want to group notifications by source like: home server, work VM, home automation, ...
- You want to send some notification to your coworkers and all notifications to yourself

Profiles allow you to mix and match between notification sources and destinations as you see fit for your use case.

Think of profiles like a pipe that can link any number of notification sources with any arbitrary notification receivers.

**Note:** `pushrocket` will use the name "default" if no profile is specified.

### Profile usage example:

In the example bellow will see who to create and use profiles:

```
$ # Create a new push profile. The following command will create testprofile.yaml in ~/.config/pushrocket/push.d
$ pushrocket -p testprofile config "tcp://localhost:1883"

$ # Share pairing string of specific profile:
$ pushrocket -p testprofile pair
<paring_string_printed>

$ # You don't need to specify the name of the profile when configuring pullrocket with the pairing string because the profile name is automatically detected.
$ pullrocket pair '<pairing_string_printed>'

$ # When listening with pullrocket, all configured profiles will be loaded.
$ pullrocket pull

$ # Send a notification using on a specific profile:
$ pushrocket -p testprofile push {your json formatted notification}
```

## FAQ:

- Where is the Android client?

Currently, android support is implemented in pushrocket for pairing: you can output the pairing string as qrcode (see `pushrocket pair -h`). The actual android app is still work in progress.

- I want an API/Http interface like pushjet/pushbullet.

It is very easy to add a http wrapper with your our authentication/authorisation layer on top of pushrocket as is.

I have chose to follow the [Unix philosophy](https://en.wikipedia.org/wiki/Unix_philosophy): Do One Thing and Do It Well.

This tool will do one thing but still far from doing it well as I have explained: this is more of a POC.

I am also open to adding a http layer on top of this if there is real demand for it but, I prefer to keep the code as simple and minimal as possible.

- I want to get/send notifications from my browser, Raspberry pi with a push button and an LCD display, my rooted smart tv set (Yes, some "smart" TVs run android OS), (put more weird stuff here), ...

You have multiple options: You can try and compile this code for your device. If this is not possible (Not supported yet) you can reimplement the sending receiving protocol for that device in native code.

- Is this a reliable message delivery system? (aka: will I ever miss notifications if I use this)

Facebook messenger [uses MQTT](https://mqtt.org/2011/08/mqtt-used-by-facebook-messenger). But, you need to know that [there is limits](https://www.electricmonk.nl/log/2017/02/20/reliable-message-delivery-with-mosquitto-mqtt/):

> * Exchanging messages in this way is obviously slower than having no consistency checks in place.
> * Since the Mosquitto broker only writes the in-memory database to disk every X (where X is configurable) seconds, you may lose data if the broker crashes.
> * On the consumer side, it is the MQTT library that confirms the receipt of the message. However, as far as I can tell, there is no way to manually confirm the receipt of a message. So if your client crashes while handling a message, rather than while it is receiving a message, you may still lose the message. If you wish to handle this case, you can store the message on the client as soon as possible. This is, however, not much more reliable. The only other way is to implement some manual protocol via the exchange of messages where the original publisher retains a message and resends it unless its been acknowledged by the consumer.

Currently, considering to migrate to [this fork](https://github.com/meshifyiot/paho.mqtt.golang/) of mqtt go lib because it has support for [deferred message ack](https://github.com/meshifyiot/paho.mqtt.golang/pull/1).

## What is next?

By priority:

- Write an android client.
- Add more documentation: especially the underlying protocol used.
- Add some unit testing.
- Add an API/http tool (?)
- Add more libraries mainly in python and javascript that you can use to build a client for your device.

## Want to contribute?

Please, feel free to add PRs or if you want ot maintain this with me then you can get in touch on my email: aminehmida --attt-- gmmmail --com--

## Author:

Amine Hmida

## Licence:

GPLv3: See [LICENSE](./LICENSE)