package cmd

import (
	"fmt"
	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"os"
	"path/filepath"
)

const (
	etcProfileDir  = "/etc/pushrocket/push.d"
	homeProfileDir = ".config/pushrocket/push.d"
)

var (
	profilePath, profileName, fullHomeProfileDir string
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "pushrocket",
	Short: "Self hosted and encrypted notification utility that uses MQTT under the hood",
	Long: `pushrocket uses profiles to be able to know how to send notification and to whom.

Each notification profile should provide all needed details for the clients to subscribe to it.
When you want to send a notification, you need to select a profilePath. If no profilePath was provided, "default" will be used.

Try pushrocket config -h to find out how to create a new profilePath.`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.PersistentFlags().StringVar(&profilePath, "profile-path", "", "Explicit profile file path. No need to provide any other profile related flags if this is set")
	rootCmd.PersistentFlags().StringVarP(&profileName, "profile", "p", "default", "Profile name to use with all actions. Need an associated '<profile>.yaml' file in profile directory")

}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if profilePath != "" {
		// Use file from the flag.
		viper.SetConfigFile(profilePath)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println("Can not find home directory: ", err.Error())
			os.Exit(1)
		}
		fullHomeProfileDir = filepath.Join(home, homeProfileDir)

		viper.AddConfigPath(etcProfileDir)
		viper.AddConfigPath(fullHomeProfileDir)
		viper.SetConfigName(profileName)
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err != nil {
		fmt.Println("Can not find or read profile file.")
	}
}
