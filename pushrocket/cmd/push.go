package cmd

import (
	"encoding/json"
	"fmt"
	"github.com/denisbrodbeck/machineid"
	"github.com/satori/go.uuid"
	"github.com/spf13/viper"
	"gitlab.com/ahmida/pushrocket/crypto"
	"gitlab.com/ahmida/pushrocket/utils"
	"os"
	"syscall"
	"time"

	"github.com/eclipse/paho.mqtt.golang"
	"github.com/spf13/cobra"
)

const pusherAppId = "PushRocketPusher"

// pushCmd represents the push command
var pushCmd = &cobra.Command{
	Use:   `push {"title": "Download complete", "message": "Your big file download is complete. Enjoy."}`,
	Short: "Push notification to clients",
	Long: `Send push notification to clients using specified profile. If no profile is specified, "default" is used
Notification is a command argument and need to be a json string with the following format:
    {
        "title": "Notification title. Can be empty.",
        "icon": "https://url-to-icon.png",
        "message": "Notification message. Can not be empty."
    }`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			fmt.Println("Message payload argument must be provided.")
			os.Exit(1)
		}

		var notification utils.Notification

		if err := json.Unmarshal([]byte(args[0]), &notification); err != nil {
			println("Error parsing notification json format: ", err.Error())
			syscall.Exit(1)
		}
		u, err := uuid.NewV4()
		if err != nil {
			println("Error generating new uuid: ", err.Error())
			syscall.Exit(1)
		}
		notification.Id = u.String()
		notification.SchemaVersion = 1
		notification.DateTime = time.Now().Format(time.RFC3339)

		notificationB, err := json.Marshal(notification)
		if err != nil {
			fmt.Println("Can not marshall json notification: ", err.Error())
		}

		if viper.GetString("address") == "" ||
			viper.GetString("topic") == "" ||
			viper.GetString("client_pub_key") == "" ||
			viper.GetString("server_priv_key") == "" {
			fmt.Println("At least 'address', 'topic', 'source-name', 'server_priv_key' and 'client_pub_key' must be set in profile config.")
			os.Exit(1)
		}

		key, err := crypto.StringToKey(viper.GetString("server_priv_key"))
		if err != nil {
			println("Can not parse 'server_priv_key': ", err.Error())
			os.Exit(1)
		}

		// Server key pair: We only need private key for encryption
		skp := crypto.Keypair{Private: *key}

		key, err = crypto.StringToKey(viper.GetString("client_pub_key"))
		if err != nil {
			println("Can not parse 'client_pub_key': ", err.Error())
			os.Exit(1)
		}

		encrypted, err := skp.Encrypt(key, notificationB)
		if err != nil {
			println("Can not encrypt message: ", err.Error())
			os.Exit(1)
		}

		secureAppID, err := machineid.ProtectedID(pusherAppId)
		if err != nil {
			fmt.Println("Can not generate unique ID for mqtt using default: ", err.Error())
			secureAppID = pusherAppId
		}

		opts := mqtt.NewClientOptions().AddBroker(viper.GetString("address")).SetClientID(secureAppID)
		opts.SetKeepAlive(2 * time.Second)
		opts.SetPingTimeout(1 * time.Second)
		c := mqtt.NewClient(opts)
		if token := c.Connect(); token.Wait() && token.Error() != nil {
			panic(token.Error())
		}
		token := c.Publish(viper.GetString("topic"), 2, false, encrypted)
		token.Wait()
	},
}

func init() {
	rootCmd.AddCommand(pushCmd)
}
