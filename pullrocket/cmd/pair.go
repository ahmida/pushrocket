package cmd

import (
	"fmt"
	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"gitlab.com/ahmida/pushrocket/utils"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"syscall"
)

var (
	profilePath, fullHomeProfileDir string
)

// pairCmd represents the pair command
var pairCmd = &cobra.Command{
	Use:   "pair",
	Short: "Add new profile config",
	Long: `Add a new notification profile config file from pairing string. 
This will parse the pairing string and add a new notification profile based on it.
No need to define profile name because it is included in the pairing information.`,
	Run: func(cmd *cobra.Command, args []string) {
		pairParser := regexp.MustCompile("\\A\\[pr,(\\d+),([a-z]+://.*:\\d+),(.*),(.*),(.+),([A-Za-z0-9+=/]{44}),([A-Za-z0-9+=/]{44}),(.+),(.*)]\\z")
		allMatches := pairParser.FindAllStringSubmatch(string(args[0]), -1)
		if len(allMatches) != 1 {
			fmt.Println("invalid pairing string")
			os.Exit(1)
		}
		matches := allMatches[0]
		if len(matches) != 10 {
			fmt.Println("invalid pairing string")
			os.Exit(1)
		}

		profileName := matches[8] // See pushrocket/cmd/pair.go to find more about the pairing format.

		if _, err := os.Stat(etcProfileDir); !os.IsNotExist(err) {
			profilePath = filepath.Join(etcProfileDir, profileName+".yaml")
		} else {
			home, err := homedir.Dir()
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
			fullHomeProfileDir = filepath.Join(home, homeProfileDir)

			fmt.Printf("Profile directoy %s does not exist. Falling back to %s\n", etcProfileDir, fullHomeProfileDir)
			err = os.MkdirAll(fullHomeProfileDir, 0770)
			if err != nil {
				fmt.Printf("Can not create profile dir %s. Exiting.\n", fullHomeProfileDir)
			}
			profilePath = filepath.Join(fullHomeProfileDir, profileName+".yaml")
		}

		profile := utils.Profile{
			Address:        matches[2],
			ClientUser:     matches[3],
			ClientPassword: matches[4],
			Topic:          matches[5],
			ClientPrivKey:  matches[6],
			ServerPubKey:   matches[7],
			ProfileIconURL: matches[9],
			Command:        cmd.Flag("cmd").Value.String(),
		}
		p, err := yaml.Marshal(profile)
		if err != nil {
			println("Error converting values to profile config")
			syscall.Exit(1)
		}
		err = ioutil.WriteFile(profilePath, p, 0600)
		if err != nil {
			println("Error saving profile config: ", err.Error())
			syscall.Exit(1)
		}
	},
}

func init() {
	rootCmd.AddCommand(pairCmd)
	pairCmd.PersistentFlags().String("cmd", "echo {{date_time}} - {{id}} [{{profile_name}}]: {{title}} {{message}}", `Command to be executed when a new message is received
The following template string will be replaced by correct values:
  * {{date_time}}         Date time when the notification was sent (not received). Using RFC3339 format.
  * {{id}}                Notification unique ID.
  * {{profile}}           Notification associated profile.
  * {{notif_icon_url}}    Associated notification icon url.
  * {{notif_icon_url}}    Associated notification profile icon url.
  * {{title}}             Notification title.
  * {{message}}           Notification message
`)
}
