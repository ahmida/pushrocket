package crypto

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"golang.org/x/crypto/nacl/box"
)

type Keypair struct {
	Public  [32]byte
	Private [32]byte
}

func (k *Keypair) Generate() (err error) {
	var pub, priv *[32]byte
	pub, priv, err = box.GenerateKey(rand.Reader)
	if err != nil {
		return
	}
	k.Public = *pub
	k.Private = *priv
	return
}

func (k *Keypair) PublicString() string {
	return base64.StdEncoding.EncodeToString(k.Public[:])
}

func (k *Keypair) PrivateString() string {
	return base64.StdEncoding.EncodeToString(k.Private[:])
}

func (k *Keypair) Encrypt(receiverPub *[32]byte, payload []byte) ([]byte, error) {
	nonce, err := genNonce()
	if err != nil {
		return nil, err
	}
	Encrypted := box.Seal(nonce[:], payload, &nonce, receiverPub, &k.Private)
	return Encrypted, nil
}

func (k *Keypair) Decrypt(senderPub *[32]byte, payload []byte) ([]byte, bool) {
	var nonce [24]byte
	copy(nonce[:], payload[:24])
	return box.Open(nil, payload[24:], &nonce, senderPub, &k.Private)
}

func StringToKey(key string) (*[32]byte, error) {
	c, err := base64.StdEncoding.DecodeString(key)
	if err != nil {
		return nil, err
	}

	if len(c) != 32 {
		return nil, fmt.Errorf("Invalid key (wrong size)")
	}
	var ret [32]byte
	copy(ret[:], c)
	return &ret, nil
}

func genNonce() (nonce [24]byte, err error) {
	var n int
	n, err = rand.Read(nonce[0:24])
	if err != nil {
		return
	}
	if n != 24 {
		err = fmt.Errorf("not enough bytes returned from rand.Reader")
	}
	return
}
