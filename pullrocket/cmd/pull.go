package cmd

import (
	"fmt"
	"github.com/denisbrodbeck/machineid"
	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"gitlab.com/ahmida/pushrocket/utils"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
)

const (
	etcProfileDir  = "/etc/pushrocket/pull.d"
	homeProfileDir = ".config/pushrocket/pull.d"
	pullerAppId    = "PushRocketPuller"
)

// pullCmd represents the pull command
var pullCmd = &cobra.Command{
	Use:   "pull",
	Short: "Parse all profiles and pull for notifications on those profiles.",
	Long: `Launch pullrocket in continues pulling mode
This will launch pullrocket as a continuously running process that will subscribe to all mqtt brokers and wait for incoming messages.
Once a new message is received, pullrocket will decrypt it, parse it and execute the command template defined when running pair command.`,
	Run: func(cmd *cobra.Command, args []string) {
		profileDir := cmd.Flag("profile-dir").Value.String()
		if profileDir == "" {
			if _, err := os.Stat(etcProfileDir); !os.IsNotExist(err) {
				profileDir = etcProfileDir
			} else {
				home, err := homedir.Dir()
				if err != nil {
					fmt.Println("Can not find home directory: ", err.Error())
					os.Exit(1)
				}
				profileDir = filepath.Join(home, homeProfileDir)
				if _, err := os.Stat(profileDir); os.IsNotExist(err) {
					fmt.Printf("Can not find valid config dir in %s and %s.", etcProfileDir, profileDir)
					os.Exit(1)
				}
			}
		}

		profilesFiles, err := filepath.Glob(profileDir + "/*.yaml")
		if err != nil {
			fmt.Printf("Can not list profilesFiles in %s: %s\n", profileDir, err.Error())
			os.Exit(1)
		}

		var profiles []utils.Profile
		var profileName string
		for _, f := range profilesFiles {
			// fmt.Println("==> Loading profile from: " + f)
			profileName = strings.Split(filepath.Base(f), ".")[0]
			profileYaml, err := ioutil.ReadFile(f)
			if err != nil {
				fmt.Printf("Can not file in %s: %s\n", f, err.Error())
				os.Exit(1)
			}
			var p utils.Profile

			if err := yaml.Unmarshal(profileYaml, &p); err != nil {
				fmt.Println("Can not unmarshal yaml profile. Check file format: ", err.Error())
			}
			p.ProfileName = profileName

			secureAppID, err := machineid.ProtectedID(pullerAppId + p.ProfileName)
			if err != nil {
				fmt.Println("Can not generate unique ID for mqtt using default: ", err.Error())
				secureAppID = pullerAppId
			}
			if err := p.Listen(secureAppID); err != nil {
				fmt.Printf("Can not listen to profile %s: %s\n", p.ProfileName, err.Error())
			}
			profiles = append(profiles, p)
		}

		c := make(chan os.Signal, 1)
		signal.Notify(c)
		// fmt.Println("Waiting for signal")
		for true {
			s := <-c
			// fmt.Println("Got signal:", s)
			if s == syscall.SIGUSR1 {
				// fmt.Println("Reloading config")
			} else if s == syscall.SIGINT || s == syscall.SIGHUP || s == syscall.SIGQUIT || s == syscall.SIGABRT {
				// fmt.Println("Cleaning up before closing app")
				for _, p := range profiles {
					p.MqttListener.Disconnect(250)
				}
				os.Exit(0)
			}
		}
	},
}

func init() {

	rootCmd.AddCommand(pullCmd)
	pullCmd.PersistentFlags().StringVar(&profilePath, "profile-dir", "", "Set explicit profiles directory. By default will check '/etc/pushrocket/pull.d' then '$HOME/.config/pushrocket/pull.d' if this flag is not set.")
}
