package cmd

import (
	"fmt"
	"github.com/spf13/viper"
	"gitlab.com/ahmida/pushrocket/crypto"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
	"path/filepath"
	"syscall"

	"github.com/spf13/cobra"
)

// ToDo: Merge this with profile in utils.go
type profile struct {
	Address        string `yaml:"address"`
	Topic          string `yaml:"topic"`
	User           string `yaml:"user"`
	Password       string `yaml:"password"`
	ClientUser     string `yaml:"client_user"`
	ClientPassword string `yaml:"client_password"`
	ProfileIconURL string `yaml:"profile_icon_url"`
	ClientPubKey   string `yaml:"client_pub_key"`
	ClientPrivKey  string `yaml:"client_priv_key"`
	ServerPubKey   string `yaml:"server_pub_key"`
	ServerPrivKey  string `yaml:"server_priv_key"`
}

// configCmd represents the config command
var configCmd = &cobra.Command{
	Use:   "config",
	Short: "Add new profilePath config",
	Long: `Add a new notification config profile 
You can add as many notification profiles as you want.

You need to create at least one notification profile called "default".
Check needed parameters bellow.`,
	Run: func(cmd *cobra.Command, args []string) {
		profilePath := viper.ConfigFileUsed()
		if profilePath == "" {
			if _, err := os.Stat(etcProfileDir); !os.IsNotExist(err) {
				profilePath = filepath.Join(etcProfileDir, cmd.Flag("profile").Value.String()+".yaml")
			} else {
				fmt.Printf("Profile directoy %s does not exist. Falling back to %s\n", etcProfileDir, fullHomeProfileDir)
				err := os.MkdirAll(fullHomeProfileDir, 0770)
				if err != nil {
					fmt.Printf("Can not create profile dir %s. Exiting.\n", fullHomeProfileDir)
				}
				profilePath = filepath.Join(fullHomeProfileDir, cmd.Flag("profile").Value.String()+".yaml")
			}
		}
		if cmd.Flag("address").Value.String() == "" {
			println("You need to provide at least MQTT broker address with '--address' flag")
			os.Exit(1)
		}

		topic := cmd.Flag("topic").Value.String()
		if topic == "" {
			topic = "pushrocket/" + cmd.Flag("profile").Value.String()
		}
		clientKeyPair := new(crypto.Keypair)
		clientKeyPair.Generate()
		serverKeyPair := new(crypto.Keypair)
		serverKeyPair.Generate()

		profile := profile{
			Address:        cmd.Flag("address").Value.String(),
			Topic:          topic,
			User:           cmd.Flag("user").Value.String(),
			Password:       cmd.Flag("password").Value.String(),
			ClientUser:     cmd.Flag("client-user").Value.String(),
			ClientPassword: cmd.Flag("client-password").Value.String(),
			ProfileIconURL: cmd.Flag("profile-icon-url").Value.String(),
			ClientPrivKey:  clientKeyPair.PrivateString(),
			ClientPubKey:   clientKeyPair.PublicString(),
			ServerPrivKey:  serverKeyPair.PrivateString(),
			ServerPubKey:   serverKeyPair.PublicString(),
		}
		// ToDo: Add this as struct function after merging in utils.go
		p, err := yaml.Marshal(profile)
		if err != nil {
			println("Error converting values to profile config")
			syscall.Exit(1)
		}
		err = ioutil.WriteFile(profilePath, p, 0600)
		if err != nil {
			println("Error saving profile config: ", err.Error())
			syscall.Exit(1)
		}

	},
}

func init() {
	rootCmd.AddCommand(configCmd)
	configCmd.PersistentFlags().String("address", "", "MQTT broker address. Example: 'tcp://myserver:1883'")
	configCmd.PersistentFlags().String("topic", "", "MQTT topic. (Default: pushrocket/<profile_name>)")
	configCmd.PersistentFlags().String("user", "", "MQTT user with publish rights to the topic")
	configCmd.PersistentFlags().String("password", "", "MQTT password for user with publish rights to the topic")
	configCmd.PersistentFlags().String("client-user", "", "MQTT user with subscribe rights to the topic")
	configCmd.PersistentFlags().String("client-password", "", "MQTT password for user with subscribe rights to the topic")
	configCmd.PersistentFlags().String("profile-icon-url", "", "Icon link of the topic")
}
